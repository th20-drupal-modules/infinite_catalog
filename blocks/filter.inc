<?php

$plugin = array(
  'name' => 'filter',
  'info' => t('Catalog filters form'),
  'view' => 'infinite_catalog_blocks_filter_view',
);

function infinite_catalog_blocks_filter_view() {
  drupal_add_js(drupal_get_path('module', 'infinite_catalog') . '/js/infinite_catalog.js');
  $form = drupal_get_form('infinite_catalog_filter_form');

  return array(
    'subject' => '',
    'content' => $form,
  );
}

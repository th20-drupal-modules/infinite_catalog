<?php

namespace Th20\InfiniteCatalog\Component;

use EntityFieldQuery;
use SelectQuery;

use Th20\InfiniteCatalog\Component\FilterComponentInterface;
use Th20\InfiniteCatalog\Configuration;


abstract class AbstractCallbackComponent implements ComponentInterface
{

    abstract public function alterQuery(SelectQuery $query, Configuration $configuration);


    public function applyCondition(EntityFieldQuery $query, Configuration $configuration)
    {
        $instance = $this;
        $value = $this->getValue();
        $query->metaData['infinite_catalog_alter_callbacks'][] = function ($query) use ($instance, $configuration) {
            return $instance->alterQuery($query, $configuration);
        };
    }

}

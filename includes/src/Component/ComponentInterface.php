<?php

namespace Th20\InfiniteCatalog\Component;

use EntityFieldQuery;

use Th20\InfiniteCatalog\Configuration;


interface ComponentInterface
{

    public function getKey();

    public function getValue();

    public function getFirstValue();

    public function setValue($value);

    public function applyCondition(EntityFieldQuery $query, Configuration $configuration);

    public function __toString();

}

<?php

namespace Th20\InfiniteCatalog\Component\Filter;

use EntityFieldQuery;

use Th20\InfiniteCatalog\Component\FilterComponentInterface;
use Th20\InfiniteCatalog\Configuration;


class BooleanFilterComponent implements FilterComponentInterface
{

    protected $key;

    protected $value;


    public function __construct($key, array $value)
    {
        $this->key = $key;
        $this->value = $value;
    }

    public function getKey()
    {
        return $this->key;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function getFirstValue()
    {
        $value = $this->getValue();

        if (is_array($value)) {
            return array_shift($value);
        } else {
            return null;
        }
    }

    public function setValue($value)
    {
        if (!$value) {
            $this->value = null;
        } elseif (!is_array($value)) {
            $this->value = array($value);
        } else {
            $this->value = $value;
        }
    }

    public function applyCondition(EntityFieldQuery $query, Configuration $configuration)
    {
        $field = $configuration->getComponentConfig($this->getKey(), 'field', $this->getKey());

        $value = $this->getFirstValue();
        if (isset($value)) {
            $query->fieldCondition($field, 'value', $value ? 1 : 0);
        }
    }

    public function getCounterClass()
    {
        return null;
    }

    public function __toString()
    {
        $value = $this->getFirstValue();
        if (isset($value)) {
            return sprintf('%s/%s', $this->getKey(), $value ? 1 : 0);
        } else {
            return '';
        }
    }

}

<?php

namespace Th20\InfiniteCatalog\Component\Filter;

use EntityFieldQuery;

use Th20\InfiniteCatalog\Component\FilterComponentInterface;
use Th20\InfiniteCatalog\Configuration;


class LanguageFilterComponent implements FilterComponentInterface
{

    protected $key;


    public function __construct($key, array $value)
    {
        $this->key = $key;
    }

    public function getKey()
    {
        return $this->key;
    }

    public function getValue()
    {
        return array();
    }

    public function getFirstValue()
    {
        return null;
    }

    public function setValue($value)
    {
    }

    public function applyCondition(EntityFieldQuery $query, Configuration $configuration)
    {
        global $language;

        $query->propertyCondition('language', $language->language);
    }

    public function getCounterClass()
    {
        return null;
    }

    public function __toString()
    {
        return '';
    }

}

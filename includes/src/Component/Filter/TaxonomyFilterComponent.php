<?php

namespace Th20\InfiniteCatalog\Component\Filter;

use EntityFieldQuery;

use Th20\InfiniteCatalog\Component\FilterComponentInterface;
use Th20\InfiniteCatalog\Configuration;


class TaxonomyFilterComponent implements FilterComponentInterface
{

    protected $key;

    protected $value;


    public function __construct($key, array $value)
    {
        $this->key = $key;
        $this->value = $value;
    }

    public function getKey()
    {
        return $this->key;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function getFirstValue()
    {
        $value = $this->getValue();

        if (is_array($value)) {
            return array_shift($value);
        } else {
            return null;
        }
    }

    public function setValue($value)
    {
        if (!$value) {
            $this->value = null;
        } elseif (!is_array($value)) {
            $this->value = array($value);
        } else {
            $this->value = $value;
        }
    }

    public function applyCondition(EntityFieldQuery $query, Configuration $configuration)
    {
        $field = $configuration->getComponentConfig($this->getKey(), 'field', $this->getKey());

        if ($value = $this->getValue()) {
            if (count($value) > 1) {
                $query->fieldCondition($field, 'tid', $value, 'IN');
            } else {
                $query->fieldCondition($field, 'tid', $value);
            }
        }
    }

    public function getCounterClass()
    {
        return 'Th20\InfiniteCatalog\Counter\TaxonomyCounter';
    }

    public function __toString()
    {
        if ($this->getValue()) {
            return sprintf('%s/%s', $this->getKey(), implode('-', $this->getValue()));
        } else {
            return '';
        }
    }

}

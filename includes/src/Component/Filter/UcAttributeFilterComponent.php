<?php

namespace Th20\InfiniteCatalog\Component\Filter;

use EntityFieldQuery;
use SelectQuery;

use Th20\InfiniteCatalog\Component\AbstractCallbackComponent;
use Th20\InfiniteCatalog\Component\FilterComponentInterface;
use Th20\InfiniteCatalog\Configuration;


class UcAttributeFilterComponent extends AbstractCallbackComponent implements FilterComponentInterface
{

    protected $key;

    protected $value;


    public function __construct($key, $value)
    {
        $this->key = $key;
        $this->value = $value;
    }

    public function getKey()
    {
        return $this->key;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function getFirstValue()
    {
        $value = $this->getValue();

        if (is_array($value)) {
            return array_shift($value);
        } else {
            return null;
        }
    }

    public function setValue($value)
    {
        if (!$value) {
            $this->value = null;
        } elseif (!is_array($value)) {
            $this->value = array($value);
        } else {
            $this->value = $value;
        }
    }

    public function getCounterClass()
    {
        return 'Th20\InfiniteCatalog\Counter\UcAttributeCounter';
    }

    public function __toString()
    {
        if ($this->getValue()) {
            return sprintf('%s/%s', $this->getKey(), implode('-', $this->getValue()));
        } else {
            return '';
        }
    }

    public function alterQuery(SelectQuery $query, Configuration $configuration)
    {
        if ($this->value) {
            $alias = 'options_' . $this->getKey();
            $query->innerJoin('uc_product_options', $alias, "$alias.nid = node.nid");
            $query->condition("$alias.oid", $this->value, 'IN');
        }
    }

}

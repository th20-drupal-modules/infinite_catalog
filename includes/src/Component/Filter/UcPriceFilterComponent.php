<?php

namespace Th20\InfiniteCatalog\Component\Filter;

use EntityFieldQuery;
use SelectQuery;

use Th20\InfiniteCatalog\Component\AbstractCallbackComponent;
use Th20\InfiniteCatalog\Component\FilterComponentInterface;
use Th20\InfiniteCatalog\Configuration;


class UcPriceFilterComponent extends AbstractCallbackComponent implements FilterComponentInterface
{

    protected $key;

    protected $value;


    public function __construct($key, $value)
    {
        $this->key = $key;
        $this->value = $value;
    }

    public function getKey()
    {
        return $this->key;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function getFirstValue()
    {
        $value = $this->getValue();

        if (is_array($value)) {
            return array_shift($value);
        } else {
            return null;
        }
    }

    public function setValue($value)
    {
        if (!$value) {
            $this->value = null;
        } elseif (!is_array($value)) {
            $this->value = array($value);
        } else {
            $this->value = $value;
        }
    }

    public function getCounterClass()
    {
        return null;
    }

    public function __toString()
    {
        if ($this->getValue()) {
            return sprintf('%s/%s', $this->getKey(), implode('-', $this->getValue()));
        } else {
            return '';
        }
    }

    public function alterQuery(SelectQuery $query, Configuration $configuration)
    {
        $value = $this->getValue();

        if ($value) {
            $alias = 'products_' . $this->getKey();
            $query->innerJoin('uc_products', $alias, "$alias.vid = node.vid");

            if (count($value) == 2) {
                $query->condition("$alias.sell_price", $value, 'BETWEEN');
            } elseif (!empty($value[0])) {
                $query->condition("$alias.sell_price", $value[0], '>=');
            }
        }
    }

}

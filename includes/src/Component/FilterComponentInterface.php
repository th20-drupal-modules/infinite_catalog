<?php

namespace Th20\InfiniteCatalog\Component;


interface FilterComponentInterface extends ComponentInterface
{

    public function getCounterClass();

}

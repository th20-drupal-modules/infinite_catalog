<?php

namespace Th20\InfiniteCatalog\Component;

use EntityFieldQuery;

use Th20\InfiniteCatalog\Configuration;


class ParentComponent implements ParentComponentInterface
{

    protected $key;

    protected $value;


    public function __construct($key, array $value)
    {
        $this->key = $key;
        $this->value = $value;
    }

    public function getKey()
    {
        return $this->key;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function getFirstValue()
    {
        $value = $this->getValue();

        if (is_array($value)) {
            return array_shift($value);
        } else {
            return null;
        }
    }

    public function setValue($value)
    {
        if (!$value) {
            $this->value = null;
        } elseif (!is_array($value)) {
            $this->value = array($value);
        } else {
            $this->value = $value;
        }
    }

    public function applyCondition(EntityFieldQuery $query, Configuration $configuration)
    {
        $name = $configuration->getComponentConfig($this->getKey(), 'vocabulary', 'catalog');

        if (!($vocab = taxonomy_vocabulary_machine_name_load($name))) {
            return;
        }

        $tids = array();
        $value = $this->getValue();

        if (!empty($value)) {
            foreach ($value as $i) {
                foreach (taxonomy_get_tree($vocab->vid, $i) as $term) {
                    $tids[] = $term->tid;
                }

                $tids[] = $i;
            }
        } else {
            foreach (taxonomy_get_tree($vocab->vid) as $term) {
                $tids[] = $term->tid;
            }
        }

        if (!empty($tids)) {
            $field = $configuration->getComponentConfig($this->getKey(), 'field', $name);
            $query->fieldCondition($field, 'tid', $tids, 'IN');
        }
    }

    public function __toString()
    {
        if ($this->getValue()) {
            return sprintf('%s/%s', $this->getKey(), implode('-', $this->getValue()));
        } else {
            return '';
        }
    }

}

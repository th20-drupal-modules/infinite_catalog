<?php

namespace Th20\InfiniteCatalog\Component\Sort;

use EntityFieldQuery;

use Th20\InfiniteCatalog\Component\SortComponentInterface;
use Th20\InfiniteCatalog\Configuration;


class FieldSortComponent implements SortComponentInterface
{

    protected $key;

    protected $value;


    public function __construct($key, $value)
    {
        $this->key = $key;
        $this->value = $value;
    }

    public function getKey()
    {
        return $this->key;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function getFirstValue()
    {
        $value = $this->getValue();

        if (is_array($value)) {
            return array_shift($value);
        } else {
            return null;
        }
    }

    public function setValue($value)
    {
        if (!$value) {
            $this->value = null;
        } elseif (!is_array($value)) {
            $this->value = array($value);
        } else {
            $this->value = $value;
        }
    }

    public function applyCondition(EntityFieldQuery $query, Configuration $configuration)
    {
        $field = $configuration->getComponentConfig($this->getKey(), 'field', $this->getKey());
        $column = $configuration->getComponentConfig($this->getKey(), 'column', 'value');

        $query->fieldOrderBy($field, $column, $this->getValue() ? 'DESC' : 'ASC');
    }

    public function __toString()
    {
        if ($this->getValue()) {
            return sprintf('%s/%s', $this->getKey(), implode('-', $this->getValue()));
        } else {
            return '';
        }
    }

}

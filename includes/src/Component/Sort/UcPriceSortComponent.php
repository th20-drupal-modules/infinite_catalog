<?php

namespace Th20\InfiniteCatalog\Component\Sort;

use SelectQuery;

use Th20\InfiniteCatalog\Component\AbstractCallbackComponent;
use Th20\InfiniteCatalog\Component\SortComponentInterface;
use Th20\InfiniteCatalog\Configuration;


class UcPriceSortComponent extends AbstractCallbackComponent implements SortComponentInterface
{

    protected $key;

    protected $value;


    public function __construct($key, $value)
    {
        $this->key = $key;
        $this->value = $value;
    }

    public function getKey()
    {
        return $this->key;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function getFirstValue()
    {
        $value = $this->getValue();

        if (is_array($value)) {
            return array_shift($value);
        } else {
            return null;
        }
    }

    public function setValue($value)
    {
        if (!$value) {
            $this->value = null;
        } elseif (!is_array($value)) {
            $this->value = array($value);
        } else {
            $this->value = $value;
        }
    }

    public function alterQuery(SelectQuery $query, Configuration $configuration)
    {
        $direction = $this->getValue() ? 'DESC' : 'ASC';

        if (module_exists('uc_product')) {
            $alias = $query->innerJoin('uc_products', 'product', 'product.vid = node.vid');
            $query->orderBy("$alias.sell_price", $direction);
        }
    }

    public function __toString()
    {
        if ($this->getValue()) {
            return sprintf('%s/%s', $this->getKey(), implode('-', $this->getValue()));
        } else {
            return '';
        }
    }

}

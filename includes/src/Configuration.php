<?php

namespace Th20\InfiniteCatalog;

use InvalidArgumentException;


class Configuration
{

    protected $config;


    public function __construct(array $config)
    {
        $this->config = $config;
    }

    public function getConfig($config, $default = null)
    {
        if (isset($this->config[$config])) {
            return $this->config[$config];
        }

        return $default;
    }

    public function getComponents()
    {
        return $this->getConfig('components', array());
    }

    public function getComponentConfig($key, $config, $default = null)
    {
        $components = $this->getComponents();

        if (!empty($components)) {
            if (!empty($components[$key])) {
                if (isset($components[$key][$config])) {
                    return $components[$key][$config];
                }
            }
        }

        return $default;
    }

}

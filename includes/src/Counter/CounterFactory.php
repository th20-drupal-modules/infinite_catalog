<?php

namespace Th20\InfiniteCatalog\Counter;

use Th20\InfiniteCatalog\Component\ComponentInterface;
use Th20\InfiniteCatalog\Component\FilterComponentInterface;


class CounterFactory
{

    private $path;


    public function __construct($path)
    {
        $this->path = $path;
    }

    public function createForComponent(ComponentInterface $component)
    {
        if (!($component instanceof FilterComponentInterface)) {
            return null;
        }

        $class = $component->getCounterClass();
        if ($class && class_exists($class)) {
            return new $class($this->path, $component);
        }

        return null;
    }

}

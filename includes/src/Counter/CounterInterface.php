<?php

namespace Th20\InfiniteCatalog\Counter;

use Th20\InfiniteCatalog\Path;


interface CounterInterface
{

    public function count($value);

}

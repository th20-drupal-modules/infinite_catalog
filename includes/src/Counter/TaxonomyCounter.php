<?php

namespace Th20\InfiniteCatalog\Counter;

use Th20\InfiniteCatalog\Path;


class TaxonomyCounter implements CounterInterface
{

    protected $path;

    protected $component;

    protected $counts;


    public function __construct(Path $path, $component)
    {
        $this->path = $path;
        $this->component = $component;
        $this->counts = null;
    }

    public function count($value)
    {
        $counts = $this->fetchCounts();

        return !empty($counts[$value]) ? $counts[$value] : 0;
    }

    protected function fetchCounts()
    {
        if (isset($this->counts)) {
            return $this->counts;
        }

        $nodes = $this->path->execute();
        if (empty($nodes)) {
            return $this->counts = array();
        }

        $nids = array();
        foreach ($nodes as $node) {
            $nids[] = $node->nid;
        }

        $fieldName = $this->path->getConfiguration()->getComponentConfig($this->component->getKey(), 'field', $this->component->getKey());
        $field = field_info_field($fieldName);
        if (empty($field)) {
            return $this->counts = array();
        }

        $columns = array_keys($field['columns']);
        $column = array_shift($columns);

        $select = db_select(_field_sql_storage_tablename($field), 'field');
        $select->addField('field', "{$field['field_name']}_tid", 'value');
        $select->addExpression('COUNT(*)', '_count');
        $select->innerJoin('node', 'node', 'node.nid = field.entity_id');
        $select->condition('node.status', 1);
        $select->condition('node.nid', $nids, 'IN');
        $select->groupBy("field.{$field['field_name']}_tid");

        return $this->counts = $select->execute()->fetchAllKeyed();
    }

}

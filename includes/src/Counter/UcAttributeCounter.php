<?php

namespace Th20\InfiniteCatalog\Counter;

use Th20\InfiniteCatalog\Component\Filter\UcAttributeFilterComponent;
use Th20\InfiniteCatalog\Path;


class UcAttributeCounter implements CounterInterface
{

    protected $path;

    protected $component;

    protected $counts;


    public function __construct(Path $path, UcAttributeFilterComponent $component)
    {
        $this->path = $path;
        $this->component = $component;
    }

    public function count($value)
    {
        $counts = $this->fetchCounts();

        return !empty($counts[$value]) ? $counts[$value] : 0;
    }

    protected function fetchCounts()
    {
        if (isset($this->counts)) {
            return $this->counts;
        }

        $nodes = $this->path->execute();
        if (empty($nodes)) {
            return $this->counts = array();
        }

        $nids = array();
        foreach ($nodes as $node) {
            $nids[] = $node->nid;
        }

        $select = db_select('uc_product_options', 'options');
        $select->addField('options', 'oid');
        $select->addExpression('COUNT(*)', '_count');
        $select->condition('options.nid', $nids, 'IN');
        $select->groupBy('options.oid');

        return $this->counts = $select->execute()->fetchAllKeyed();
    }


}

<?php

namespace Th20\InfiniteCatalog\Factory;


class ComponentFactory
{

    public function createType($type, $key, $value)
    {
        $parts = explode('.', $type);
        switch (count($parts)) {
            case 2:
                $className = sprintf(
                    'Th20\\InfiniteCatalog\\Component\\%s\\%sComponent',
                    $this->capitalize($parts[0]),
                    $this->capitalize($parts[1])
                );
                break;

            case 1:
                $className = sprintf(
                    'Th20\\InfiniteCatalog\\Component\\%sComponent',
                    $this->capitalize($parts[0])
                );
                break;

            default:
                $className = $type;
                return null;
        }

        if (class_exists($className)) {
            return new $className($key, $value);
        }

        return null;
    }

    private function capitalize($string)
    {
        $capitalized = '';

        foreach (explode('_', $string) as $part) {
            $capitalized .= strtoupper($part[0]) . substr($part, 1);
        }

        return $capitalized;
    }

}

<?php

namespace Th20\InfiniteCatalog\Factory;

use Exception;

use Th20\InfiniteCatalog\Configuration;
use Th20\InfiniteCatalog\Path;


class PathFactory
{

    protected $configuration;


    public function __construct(Configuration $configuration)
    {
        $this->configuration = $configuration;
    }

    public function blank()
    {
        return new Path($this->createComponents(), $this->configuration);
    }

    public function parse($raw)
    {
        $components = $this->createComponents();
        $parser = new PathParser($raw);

        try {
            $parsed = $parser->toArray();
        } catch (Exception $e) {
            return $this->blank();
        }

        foreach ($parsed as $key => $value) {
            if (isset($components[$key])) {
                $components[$key]->setValue($value);
            }
        }

        return new Path($components, $this->configuration);
    }

    private function createComponents()
    {
        $components = array();
        $componentsFactory = new ComponentFactory();

        foreach ($this->configuration->getComponents() as $key => $config) {
            if (empty($config['type'])) {
                continue;
            }

            if ($component = $componentsFactory->createType($config['type'], $key, array())) {
                $components[$key] = $component;
            }
        }

        return $components;
    }

}

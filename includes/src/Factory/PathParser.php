<?php

namespace Th20\InfiniteCatalog\Factory;

use Exception;


class PathParser
{

    protected $parts;


    public function __construct($path)
    {
        $this->parts = explode('/', trim(strval($path)));
    }

    public function toArray()
    {
        $parsed = array();

        while (!$this->isCompleted()) {
            $key = $this->readKey();
            $value = $this->readValue();

            $parsed[$key] = $value;
        }

        return $parsed;
    }

    public function isCompleted()
    {
        return count($this->parts) == 0;
    }

    public function readKey()
    {
        $part = $this->readPart();

        if (!preg_match('/^[a-z_]{1,30}$/', $part)) {
            throw new Exception('Component key does not match required format.');
        }

        return $part;
    }

    public function readValue()
    {
        $part = $this->readPart();

        if (!preg_match('/^[0-9-,]{1,50}$/', $part)) {
            throw new Exception('Component value is not numeric.');
        }

        return array_map('intval', explode('-', str_replace(',', '-', $part)));
    }

    protected function readPart()
    {
        if ($this->isCompleted()) {
            throw new Exception('No more path components to read.');
        }

        return array_shift($this->parts);
    }

}

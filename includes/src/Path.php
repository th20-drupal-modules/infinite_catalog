<?php

namespace Th20\InfiniteCatalog;

use Th20\InfiniteCatalog\Component\ParentComponentInterface;
use Th20\InfiniteCatalog\Counter\CounterFactory;
use Th20\InfiniteCatalog\Parser\PathParser;
use Th20\InfiniteCatalog\Search\Executor;


class Path
{

    protected $configuration;

    protected $components;

    protected $search;


    public function __construct(array $components, Configuration $configuration)
    {
        $this->components = $components;
        $this->configuration = $configuration;
        $this->search = new Executor($this);
    }

    public function __clone()
    {
        $this->search = new Executor($this);
    }

    public function isEmpty()
    {
        $isEmpty = true;

        foreach ($this->components as $component) {
            $value = $component->getValue();

            if (!empty($value)) {
                $isEmpty = false;
            }
        }

        return $isEmpty;
    }

    public function setComponent($key, $value)
    {
        foreach ($this->components as $component) {
            if ($component->getKey() == $key) {
                $component->setValue($value);
            }
        }

        return $this;
    }

    public function getComponent($key)
    {
        foreach ($this->components as $component) {
            if ($component->getKey() == $key) {
                return $component;
            }
        }

        return null;
    }

    public function getComponents()
    {
        return $this->components;
    }

    public function forceParent($parent)
    {
        foreach ($this->components as $component) {
            if ($component instanceof ParentComponentInterface) {
                $component->setValue($parent);
            }
        }
    }

    public function getParent()
    {
        foreach ($this->components as $component) {
            if ($component instanceof ParentComponentInterface) {
                return $component->getValue();
            }
        }

        return array();
    }

    public function getConfiguration()
    {
        return $this->configuration;
    }

    public function execute()
    {
        return $this->search->execute();
    }

    public function getCounter($key)
    {
        $factory = new CounterFactory($this);

        return $factory->createForComponent($this->getComponent($key));
    }

    public function __toString()
    {
        return implode('/', array_filter(array_map('strval', $this->components)));
    }

}

<?php

namespace Th20\InfiniteCatalog\Search;

use Th20\InfiniteCatalog\Path;


class Executor
{

    protected $path;

    protected $result;


    public function __construct(Path $path)
    {
        $this->path = $path;
        $this->result = null;
    }

    public function execute()
    {
        if (isset($this->result)) {
            return $this->result;
        }

        $configuration = $this->path->getConfiguration();

        $query = entity_field_query('node');
        $query->addTag('infinite_catalog_search');
        $query->metaData['infinite_catalog_alter_callbacks'] = array();

        $query->propertyCondition('status', 1);

        foreach ($this->path->getComponents() as $component) {
            $component->applyCondition($query, $configuration);
        }

        if ($pagination = intval($configuration->getConfig('pagination'))) {
            $query->pager($pagination);
        }

        $entities = $query->execute();
        if (empty($entities['node'])) {
            return $this->result = array();
        }

        $nids = $this->extractField($entities['node'], 'nid');
        $nodes = node_load_multiple($nids);

        return $this->result = $nodes;
    }

    private function extractField($list, $field)
    {
        $data = array();

        foreach ($list as $key => $obj) {
            if (isset($obj->$field)) {
                $data[$key] = $obj->$field;
            }
            else {
                $data[$key] = null;
            }
        }

        return $data;
    }

}

<?php

/**
 * @file
 * Drupal menu page callbacks.
 */

function infinite_catalog_page() {
  $path = infinite_catalog_active_path();
  $nodes = $path->execute();

  if ($parent = $path->getParent()) {
    if ($term = taxonomy_term_load($parent[0])) {
      drupal_set_title($term->name);
    }
  }

  if (empty($nodes)) {
    return array(
      '#theme' => 'infinite_catalog_empty',
    );
  }

  $view = node_view_multiple($nodes, 'teaser');

  return array(
    'nodes' => array(
      '#theme' => 'infinite_catalog_grid',
      '#items' => array_intersect_key($view['nodes'], array_flip(element_children($view['nodes']))),
      '#path' => $path,
    ),
    'pager' => array(
      '#prefix' => '<div class="infinite-catalog-pager">',
      '#theme' => 'pager',
      '#suffix' => '</div>',
    ),
  );
}

function infinite_catalog_submit_page() {
  if ($_SERVER['REQUEST_METHOD'] == 'GET' && !empty($_GET['catalog'])) {
    drupal_goto('catalog', array(
      'query' => array(
        'catalog' => $_GET['catalog'],
        'page' => !empty($_GET['page']) ? $_GET['page'] : 0,
      ),
    ));
  }

  $form_state = array(
    'build_info' => array('args' => array()),
    'no_redirect' => TRUE,
  );

  $form = drupal_build_form('infinite_catalog_filter_form', $form_state);
  $page = infinite_catalog_page();

  echo json_encode(array(
    'grid' => render($page),
    'form' => render($form),
  ));
}

function infinite_catalog_taxonomy_term_page($term) {
  module_load_include('inc', 'taxonomy', 'taxonomy.pages');

  $vocab = taxonomy_vocabulary_load($term->vid);

  $path = infinite_catalog_active_path();
  $configuration = $path->getConfiguration();

  foreach ($path->getComponents() as $key => $component) {
    if ($component instanceof Th20\InfiniteCatalog\Component\ParentComponentInterface) {
      $componentVocab = $configuration->getComponentConfig($key, 'vocabulary', $key);

      if ($componentVocab == $vocab->machine_name) {
        $path->forceParent($term->tid);
        drupal_goto('catalog/' . $path->getParent(), array('query' => array('catalog' => strval($path))));
      }
    }

    if ($component instanceof Th20\InfiniteCatalog\Component\Filter\TaxonomyFilterComponent) {
      $componentVocab = $configuration->getComponentConfig($key, 'vocabulary', $key);

      if ($componentVocab == $vocab->machine_name) {
        $component->setValue($term->tid);

        if ($path->getParent()) {
          drupal_goto('catalog/' . $path->getParent(), array('query' => array('catalog' => strval($path))));
        } else {
          drupal_goto('catalog', array('query' => array('catalog' => strval($path))));
        }
      }
    }
  }

  return taxonomy_term_page($term);
}

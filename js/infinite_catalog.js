;(function ($, Drupal, window, document, undefined) {

    Drupal.behaviors.infiniteCatalog = {
        form: null,
        grid: null,

        attach: function (context, settings) {
            var _this = this,
                context = $(context);

            var grid = context.find('.infinite-catalog-grid');
            if (grid.length > 0) {
                this.grid = grid;
            }

            var form = context.find('#infinite-catalog-filter-form');
            if (form.length > 0) {
                this.form = form;
                this.attachFormEvents(form, settings);
            }
        },

        attachFormEvents: function (form, settings) {
            var _this = this;

            if (this.grid) {
                form.find('.form-submit').hide();
            }

            form.find('input, textarea, select').change(function () {
                var values = form.serialize(),
                    action = form.data('action');

                if (!_this.grid) {
                    return;
                }

                $.ajax({
                    url: action,
                    type: 'POST',
                    dataType: 'json',
                    data: values
                }).done(function (data) {
                    var formContainer = _this.form.parent(),
                        gridContainer = _this.grid.parent();

                    _this.grid.siblings('.infinite-catalog-pager').remove();

                    _this.form.replaceWith(data['form']);
                    _this.grid.replaceWith(data['grid']);

                    Drupal.attachBehaviors(formContainer);
                    Drupal.attachBehaviors(gridContainer);
                });
            });
        }
    }

})(jQuery, Drupal, this, this.document);
